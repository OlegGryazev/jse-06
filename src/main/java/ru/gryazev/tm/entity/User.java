package ru.gryazev.tm.entity;

import ru.gryazev.tm.enumerated.RoleType;

import java.util.UUID;

public class User {

    private String id = UUID.randomUUID().toString();

    private String login = "";

    private String pwdHash = "";

    private RoleType roleType;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Login: " + login + "\n" +
                "Role: " + roleType;
    }

}
