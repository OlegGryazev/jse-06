package ru.gryazev.tm.entity;

import ru.gryazev.tm.util.DateUtils;

import java.util.Date;
import java.util.UUID;

public class Task {

    private String id = UUID.randomUUID().toString();

    private String userId = "";

    private String projectId = "";

    private String name = "";

    private String details = "";

    private Date dateStart;

    private Date dateFinish;

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                        "Details: %s\n" +
                        "Starts: %s\n" +
                        "Ends: %s",
                name,
                details,
                DateUtils.formatDateToString(dateStart),
                DateUtils.formatDateToString(dateFinish));
    }

}
