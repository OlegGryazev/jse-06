package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class TaskService{

    private TaskRepository taskRepository;

    private ProjectRepository projectRepository;

    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task create(String userId, String projectId, Task task) throws IOException {
        if (!isValid(projectId, userId))
            return null;
        task.setProjectId(projectId);
        if (!isTaskValid(task))
            return null;

        return taskRepository.persist(userId, task);
    }

    public Task view(String userId, String taskId) {
        if (!isValid(taskId, userId))
            return null;
        return taskRepository.findOne(userId, taskId);
    }

    public List<Task> list(String projectId, String userId) {
        if (!isValid(projectId, userId))
            return null;

        return taskRepository.findAll(userId).values().stream().filter(o ->
                o.getProjectId() != null && o.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    public List<Task> listUnlinked(String userId) {
        if (!isValid(userId))
            return null;

        return taskRepository.findAll(userId).values().stream().filter(o ->
                o.getProjectId() == null).collect(Collectors.toList());
    }

    public Task unlinkTask(String userId, Task task) {
        if (!isTaskValid(task) || !isValid(userId))
            return null;
        task.setProjectId(null);
        return taskRepository.merge(userId, task);
    }

    public Task edit(String userId, Task task) {
        if (!isTaskValid(task) || !isValid(userId))
            return null;

        return taskRepository.merge(userId, task);
    }

    public Task remove(String userId, String taskId) {
        if (!isValid(taskId, userId))
            return null;

        return taskRepository.remove(userId, taskId);
    }

    public void clear(String userId) {
        if (!isValid(userId))
            return;

        taskRepository.removeAll(userId);
    }

    public String getTaskId(String projectId, String userId, int taskIndex) {
        if (!isValid(projectId, userId))
            return null;

        List<Task> tasks = list(projectId, userId);

        if (tasks == null || !ArrayUtils.indexExists(tasks, taskIndex))
            return null;

        return tasks.get(taskIndex).getId();
    }

    private boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;

        return true;
    }

    private boolean isTaskValid(Task task) {
        if (task == null)
            return false;

        return isValid(task.getId(), task.getUserId(), task.getProjectId(), task.getName());
    }

}