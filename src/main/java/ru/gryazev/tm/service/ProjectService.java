package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectService{

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Project create(String userId, Project project) {
        if (!isProjectValid(project) || !isValid(userId))
            return null;

        return projectRepository.persist(userId, project);
    }

    public Project view(String userId, String projectId) {
        if (!isValid(userId, projectId))
            return null;
        return projectRepository.findOne(userId, projectId);
    }

    public List<Project> list(String userId) {
        if (!isValid(userId))
            return null;
        return new ArrayList<>(projectRepository.findAll(userId).values());
    }

    public Project edit(String userId, Project project) {
        if (!isProjectValid(project) || !isValid(userId))
            return null;

        return projectRepository.merge(userId, project);
    }

    public Project remove(String userId, String projectId) {
        if (!isValid(userId,projectId))
            return null;
        Project project;
        if ((project = projectRepository.remove(userId, projectId)) == null)
            return null;

        for (Map.Entry<String, Task> pair : taskRepository.findAll(userId).entrySet())
            if (pair.getValue().getProjectId().equals(projectId))
                taskRepository.remove(userId, pair.getValue().getId());

        return project;
    }

    public void clear(String userId){
        if (!isValid(userId))
            return;

        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }

    public String getProjectId(int projectIndex, String userId){
        if (!isValid(userId))
            return null;

        List<Project> projects = list(userId);

        if (projects == null || !ArrayUtils.indexExists(projects, projectIndex))
            return null;

        return projects.get(projectIndex).getId();
    }

    private boolean isProjectValid(Project project) {
        if (project == null)
            return false;

        return isValid(project.getId(), project.getName(), project.getUserId());
    }

    private boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;

        return true;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

}
