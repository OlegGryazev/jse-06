package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.repository.UserRepository;

import java.io.IOException;
import java.util.Map;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(User user) throws IOException {
        if (!isUserValid(user))
            return null;

        return userRepository.persist(user);
    }

    public String login(User user) {
        if (!isUserValid(user))
            return null;

        User loggedUser = userRepository.findOne(user.getLogin(), user.getPwdHash());

        if (loggedUser == null)
            return null;
        return loggedUser.getId();
    }

    public User edit(User user) {
        if (!isUserValid(user))
            return null;

        return userRepository.merge(user);
    }

    public User findOne(String userId) {
        if (!isValid(userId))
            return null;

        return userRepository.findOne(userId);
    }

    private boolean isUserValid(User user) {
        if (user == null || user.getRoleType() == null)
            return false;
        return isValid(user.getId(), user.getLogin(), user.getPwdHash());
    }

    private boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;

        return true;
    }

    public Map<String, User> findAll() {
        return userRepository.findAll();
    }

}
