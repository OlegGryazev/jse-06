package ru.gryazev.tm.enumerated;

public enum RoleType {

    USER("User"),
    ADMIN("Administrator");

    private String roleName;

    RoleType(String roleName) {
        this.roleName = roleName;
    }

    public String displayName(){
        return roleName;
    }

}
