package ru.gryazev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static Date inputDate(BufferedReader in){
        SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result = null;
        try {
            result = format.parse(in.readLine());
        } catch (ParseException | IOException e){
            System.out.println("Entered date is incorrect. You can edit date later.");
        }
        return result;
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    public static String formatDateToString(Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

}
