package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Task;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    public Task findOne(String userId, String id){
        Task task = tasks.get(id);
        if (task == null || !task.getUserId().equals(userId))
            return null;

        return task;
    }

    public Task merge(String userId, Task task){
        if (!task.getUserId().equals(userId))
            return null;

        tasks.put(task.getId(), task);
        return task;
    }

    public Task persist(String userId, Task task) {
        if (!task.getUserId().equals(userId))
            return null;
        if (findOne(userId, task.getId()) != null)
            return null;

        tasks.put(task.getId(), task);
        return task;
    }

    public Task remove(String userId, String id){
        if (!tasks.get(id).getUserId().equals(userId))
            return null;
        return tasks.remove(id);
    }

    public void removeAll(String userId){
        List<Task> tasksToRemove = tasks.values().stream().filter(o ->
                o.getUserId().equals(userId)).collect(Collectors.toList());
        tasksToRemove.forEach(o -> tasks.remove(o.getId()));
    }

    public Map<String, Task> findAll(String userId) {
        Map<String, Task> result = new LinkedHashMap<>();
        tasks.forEach((k, v) -> {
            if (v.getUserId().equals(userId))
                result.put(v.getId(), v);
        });
        return result;
    }

}
