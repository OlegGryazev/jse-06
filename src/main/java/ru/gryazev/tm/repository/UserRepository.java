package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.User;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {

    private Map<String, User> users = new LinkedHashMap<>();

    public User findOne(String id){
        return users.get(id);
    }

    public User findOne(String login, String pwd){
        return users.values().stream().filter(o ->
                o.getLogin().equals(login) && o.getPwdHash().equals(pwd)).findFirst().orElse(null);
    }

    public User merge(User user){
        users.put(user.getId(), user);
        return user;
    }

    public User persist(User user) throws IOException {
        if (findOne(user.getId()) != null)
            throw new IOException("User must have unique Id!");
        users.put(user.getId(), user);
        return user;
    }

    public User remove(String id){
        return users.remove(id);
    }

    public void removeAll(){
        users.clear();
    }

    public Map<String, User> findAll() {
        return users;
    }

}
