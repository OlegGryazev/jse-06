package ru.gryazev.tm.repository;

import ru.gryazev.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    public Project findOne(String userId, String id) {
        Project project = projects.get(id);
        if (project == null || !project.getUserId().equals(userId))
            return null;

        return project;
    }

    public Project merge(String userId, Project project){
        if (!project.getUserId().equals(userId))
            return null;

        projects.put(project.getId(), project);
        return project;
    }

    public Map<String, Project> findAll(String userId) {
        Map<String, Project> result = new LinkedHashMap<>();
        projects.forEach((k, v) -> {
            if (v.getUserId().equals(userId))
                result.put(v.getId(), v);
        });
        return result;
    }

    public Project persist(String userId, Project project) {
        if (!project.getUserId().equals(userId))
            return null;
        if (findOne(userId, project.getId()) != null)
            return null;
        projects.put(project.getId(), project);
        return project;
    }

    public Project remove(String userId, String id){
        if (!projects.get(id).getUserId().equals(userId))
            return null;
        return projects.remove(id);
    }

    public void removeAll(String userId){
        List<Project> projectsToRemove = projects.values().stream().filter(o ->
                o.getUserId().equals(userId)).collect(Collectors.toList());
        projectsToRemove.forEach(o -> projects.remove(o.getId()));
    }

}
