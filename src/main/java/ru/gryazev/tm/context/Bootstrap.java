package ru.gryazev.tm.context;

import ru.gryazev.tm.command.*;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.service.UserService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final ConsoleView consoleView = new ConsoleView();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private final TaskService taskService = new TaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private String selectedProjectId = null;

    private String currentUserId = null;

    public void init() {
        consoleView.print("**** Welcome to Project Manager ****");
        commands = commandsInit();
        usersInit();

        while (true) {
            try {
                AbstractCommand command = commands.get(consoleView.readCommand());

                if (command == null) {
                    consoleView.print("Command not found!");
                    continue;
                }

                if (!isUserLogged() && !command.isAllowed()) {
                    consoleView.print("Access denied!");
                    continue;
                }

                command.execute();
            } catch (RuntimeException e) {
                consoleView.print(e.getMessage());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractCommand> commandsInit() {
        Map<String, AbstractCommand> result = new LinkedHashMap<>();

        AbstractCommand[] commandArray = new AbstractCommand[]{new ExitCommand(this), new HelpCommand(this), new ProjectClearCommand(this),
                new ProjectCreateCommand(this), new ProjectEditCommand(this), new ProjectListCommand(this), new ProjectRemoveCommand(this),
                new ProjectSelectCommand(this), new ProjectViewCommand(this), new TaskClearCommand(this), new TaskClearCommand(this),
                new TaskCreateCommand(this), new TaskEditCommand(this), new TaskListCommand(this), new TaskRemoveCommand(this),
                new TaskViewCommand(this), new TaskUnlinkCommand(this), new TaskLinkCommand(this), new TaskListUnlinkedCommand(this),
                new UserLoginCommand(this), new UserLogoutCommand(this), new UserRegistrationCommand(this),
                new UserUpdateCommand(this), new UserViewCommand(this), new UserEditCommand(this)};

        for (AbstractCommand abstractCommand : commandArray) {
            result.put(abstractCommand.getName(), abstractCommand);
        }
        return result;
    }

    private void usersInit() {
        try{
            User user = new User();
            user.setLogin("user");
            user.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
            user.setRoleType(RoleType.USER);
            userService.create(user);

            User admin = new User();
            admin.setLogin("admin");
            admin.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
            admin.setRoleType(RoleType.ADMIN);
            userService.create(admin);
        } catch (IOException e){
            consoleView.print("[Error during default user initialization. You must register new user manually.");
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public ConsoleView getConsoleView() {
        return consoleView;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public String getSelectedProjectId() {
        return selectedProjectId;
    }

    public void setSelectedProjectId(String selectedProjectId) {
        this.selectedProjectId = selectedProjectId;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

    public boolean isUserLogged(){
        return currentUserId != null;
    }

}
