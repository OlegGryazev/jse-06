package ru.gryazev.tm.error;

public class CrudCreateException extends RuntimeException {

    public CrudCreateException() {
        super("Error during create operation");
    }

}
