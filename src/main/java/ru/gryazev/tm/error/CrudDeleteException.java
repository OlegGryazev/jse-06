package ru.gryazev.tm.error;

public class CrudDeleteException extends RuntimeException {

    public CrudDeleteException() {
        super("Error during delete operation");
    }

}
