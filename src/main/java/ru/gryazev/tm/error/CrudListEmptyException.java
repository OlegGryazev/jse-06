package ru.gryazev.tm.error;

public class CrudListEmptyException extends RuntimeException {

    public CrudListEmptyException() {
        super("Error: list is empty");
    }

}
