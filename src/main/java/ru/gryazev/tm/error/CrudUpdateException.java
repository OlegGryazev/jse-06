package ru.gryazev.tm.error;

public class CrudUpdateException extends RuntimeException{

    public CrudUpdateException() {
        super("Error during update operation");
    }

}
