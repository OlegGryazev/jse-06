package ru.gryazev.tm.error;

public class CrudNotFoundException extends RuntimeException {

    public CrudNotFoundException() {
        super("Error: element not found.");
    }

}
