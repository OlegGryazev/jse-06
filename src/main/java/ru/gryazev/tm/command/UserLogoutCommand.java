package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

import java.io.IOException;

public class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() {
        if (bootstrap.isUserLogged()) {
            bootstrap.setCurrentUserId(null);
            bootstrap.getConsoleView().print("You are logged out.");
        }
    }

}
