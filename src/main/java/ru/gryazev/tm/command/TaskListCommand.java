package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        String projectId = bootstrap.getSelectedProjectId();

        List<Task> tasks = bootstrap.getTaskService().list(projectId, bootstrap.getCurrentUserId());
        if (tasks == null || tasks.size() == 0)
            throw new CrudListEmptyException();

        for (int i = 0; i < tasks.size(); i++)
            consoleView.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
