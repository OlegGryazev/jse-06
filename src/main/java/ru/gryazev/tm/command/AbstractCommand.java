package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    private boolean allowed;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.allowed = false;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException;

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

}
