package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        User user = consoleView.getUserFromConsole();
        String userId;

        if ((userId = bootstrap.getUserService().login(user)) == null) {
            consoleView.print("[ERROR DURING USER LOGIN]");
            return;
        }

        consoleView.print("[" + user.getLogin() + " logged in]");
        bootstrap.setCurrentUserId(userId);
    }

}
