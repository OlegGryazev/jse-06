package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.UserService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class UserUpdateCommand extends AbstractCommand {

    public UserUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        UserService userService = bootstrap.getUserService();
        String pwd = consoleView.getPwdHashFromConsole();
        String pwdRepeat = consoleView.getPwdHashFromConsole();

        if (!pwd.equals(pwdRepeat)){
            consoleView.print("Entered passwords do not match!");
            return;
        }

        User user = userService.findOne(bootstrap.getCurrentUserId());
        user.setPwdHash(pwd);
        if (userService.edit(user) == null)
            throw new CrudUpdateException();

        consoleView.print("[OK]");
    }

}
