package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task at selected project.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        String userId = bootstrap.getCurrentUserId();
        String projectId = bootstrap.getSelectedProjectId();

        consoleView.print("[TASK CREATE]");
        Task task = consoleView.getTaskFromConsole();
        task.setUserId(userId);

        if (bootstrap.getTaskService().create(userId, projectId, task) == null)
            throw new CrudCreateException();

        consoleView.print("[OK]");
    }

}
