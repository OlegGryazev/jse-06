package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(Bootstrap bootstrap) {
        super(bootstrap);
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration of new user.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        User user = consoleView.getUserFromConsole();
        String pwdRepeat = consoleView.getPwdHashFromConsole();
        if (!user.getPwdHash().equals(pwdRepeat)){
            consoleView.print("Entered passwords do not match!");
            return;
        }
        if (bootstrap.getUserService().create(user) == null)
            throw new CrudCreateException();

        consoleView.print("[OK]");
    }

}
