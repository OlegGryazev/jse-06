package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.User;

import java.io.IOException;

public class UserViewCommand extends AbstractCommand {

    public UserViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-view";
    }

    @Override
    public String getDescription() {
        return "View user data.";
    }

    @Override
    public void execute() throws IOException {
        User user = bootstrap.getUserService().findOne(bootstrap.getCurrentUserId());
        if (user != null)
            bootstrap.getConsoleView().print(user.toString());
    }

}
