package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public class TaskListUnlinkedCommand extends AbstractCommand {

    public TaskListUnlinkedCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list-unlinked";
    }

    @Override
    public String getDescription() {
        return "Shows all unlinked tasks";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();

        List<Task> tasks = bootstrap.getTaskService().listUnlinked(bootstrap.getCurrentUserId());
        if (tasks == null || tasks.size() == 0)
            throw new CrudListEmptyException();

        for (int i = 0; i < tasks.size(); i++)
            consoleView.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
