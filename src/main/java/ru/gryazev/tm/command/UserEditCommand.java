package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.UserService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class UserEditCommand extends AbstractCommand {

    public UserEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user data.";
    }

    @Override
    public void execute() throws IOException {
        UserService userService = bootstrap.getUserService();
        ConsoleView consoleView = bootstrap.getConsoleView();
        consoleView.print("[USER EDIT]");
        User userEditData = consoleView.getUserFromConsole();
        String pwdRepeat = consoleView.getPwdHashFromConsole();
        if (!userEditData.getPwdHash().equals(pwdRepeat)){
            consoleView.print("Entered passwords do not match!");
            return;
        }
        User currentUser = userService.findOne(bootstrap.getCurrentUserId());
        userEditData.setRoleType(currentUser.getRoleType());
        userEditData.setId(currentUser.getId());

        if (userService.edit(userEditData) == null)
            throw new CrudUpdateException();

        consoleView.print("[OK]");
    }

}
