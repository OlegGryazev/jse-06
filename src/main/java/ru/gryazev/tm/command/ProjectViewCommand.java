package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.view.ConsoleView;

public class ProjectViewCommand extends AbstractCommand {

    public ProjectViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-view";
    }

    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        ProjectService projectService = bootstrap.getProjectService();

        String userId = bootstrap.getCurrentUserId();
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex, userId);
        Project project = projectService.view(userId, projectId);
        if (project == null)
            throw new CrudNotFoundException();

        consoleView.print(project.toString());
    }

}
