package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        String userId = bootstrap.getCurrentUserId();
        consoleView.print("[PROJECT CREATE]");
        Project project = consoleView.getProjectFromConsole();
        project.setUserId(userId);

        if (bootstrap.getProjectService().create(userId, project) == null)
            throw new CrudCreateException();

        consoleView.print("[OK]");
    }

}
