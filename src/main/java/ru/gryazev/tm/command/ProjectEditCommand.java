package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        ProjectService projectService = bootstrap.getProjectService();
        String userId = bootstrap.getCurrentUserId();

        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex, bootstrap.getCurrentUserId());

        if (projectId == null)
            throw new CrudNotFoundException();

        Project project = consoleView.getProjectFromConsole();
        project.setId(projectId);
        project.setUserId(userId);

        if (bootstrap.getProjectService().edit(userId, project) == null)
            throw new CrudUpdateException();

        consoleView.print("[OK]");
    }

}
