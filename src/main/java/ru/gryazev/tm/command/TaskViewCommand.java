package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

public class TaskViewCommand extends AbstractCommand {

    public TaskViewCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-view";
    }

    @Override
    public String getDescription() {
        return "View selected task.";
    }

    @Override
    public void execute() {
        ConsoleView consoleView = bootstrap.getConsoleView();
        TaskService taskService = bootstrap.getTaskService();
        String userId = bootstrap.getCurrentUserId();
        String projectId = bootstrap.getSelectedProjectId();

        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(projectId, userId, taskIndex);
        Task task = taskService.view(userId, taskId);
        if (task == null)
            throw new CrudNotFoundException();

        consoleView.print(task.toString());
    }

}
