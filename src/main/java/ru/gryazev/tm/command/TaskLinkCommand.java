package ru.gryazev.tm.command;

import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.util.ArrayUtils;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;
import java.util.List;

public class TaskLinkCommand extends AbstractCommand {

    public TaskLinkCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project";
    }

    @Override
    public void execute() throws IOException {
        ConsoleView consoleView = bootstrap.getConsoleView();
        TaskService taskService = bootstrap.getTaskService();
        ProjectService projectService = bootstrap.getProjectService();

        String userId = bootstrap.getCurrentUserId();
        String currentProjectId = bootstrap.getSelectedProjectId();
        List<Task> tasks = currentProjectId == null ?
                taskService.listUnlinked(userId) : taskService.list(currentProjectId, userId);
        int taskIndex = consoleView.getTaskIndex();
        if (tasks == null || !ArrayUtils.indexExists(tasks, taskIndex))
            throw new CrudNotFoundException();

        Task task = tasks.get(taskIndex);
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex, bootstrap.getCurrentUserId());
        task.setProjectId(projectId);

        if (taskService.edit(userId, task) == null)
            throw new CrudUpdateException();

        consoleView.print("[OK]");
    }

}
