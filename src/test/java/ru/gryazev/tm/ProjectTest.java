package ru.gryazev.tm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class ProjectTest extends Assert {

    private final Bootstrap bootstrap = new Bootstrap();

    private final Project project = new Project();

    @Before
    public void setProject(){
        project.setName("test");
        project.setDetails("details");

        SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date startDate = null;
        try {
            startDate = format.parse("02.02.2020");
        } catch (ParseException e){}

        if (startDate != null)
            project.setDateStart(startDate);

        Date endDate = null;
        try {
            endDate = format.parse("adfasdf");
        } catch (ParseException e){}
        if (endDate != null)
            project.setDateFinish(endDate);

    }

    @Test
    public void ProjectOkCreate() {
        project.setUserId(UUID.randomUUID().toString());
        assertNotNull(bootstrap.getProjectService().create(project.getUserId(), project));
    }

    @Test
    public void ProjectNullUserIdCreate() {
        project.setUserId(null);
        assertNull(bootstrap.getProjectService().create(project.getUserId(), project));
    }

    @Test
    public void ProjectDifferentUserIdCreate() {
        project.setUserId(UUID.randomUUID().toString());
        assertNull(bootstrap.getProjectService().create(UUID.randomUUID().toString(), project));
    }

}
